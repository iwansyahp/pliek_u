<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('security_model');
        //$this->security_model->getAdminSecurity();
    }
    
    public function index()
    {
        $isi['title'] = 'Dashboard Admin';
        $this->load->view('dashboard/inc/header_view',$isi);
        $this->load->view('dashboard/dashboard_view');
        $this->load->view('dashboard/inc/footer_view');
    }
    
    public function daftar_user()
    {
        $isi['title'] = 'Daftarkan User';
        $this->load->view('dashboard/inc/header_view',$isi);
        $this->load->view('dashboard/daftar_user_view');
        $this->load->view('dashboard/inc/footer_view');
    }
    
    public function daftar_apus()
    {
        //$username = $this->input->post('username');
        
        //$data = array(
        //    'username' => $username,
        //);
        $isi['title'] = 'Form Pendaftaran Anak Putus Sekolah';
        $this->load->view('dashboard/inc/header_view',$isi);
        $this->load->view('dashboard/form_daftar_apus_view');
        $this->load->view('dashboard/inc/footer_view');
        //redirect('home/login');
    }
    
    public function edit_apus()
    {
        $isi['title'] = 'Edit Data Anak Putus Sekolah';
        $this->load->view('dashboard/inc/header_view',$isi);
        $this->load->view('dashboard/edit_apus_view');
        $this->load->view('dashboard/inc/footer_view');
    }
    
    public function delete_apus($id)
    {
        $this->load->model('data_model');
        $this->data_model->delete_apus($id);
    }
    
    public function edit_user()
    {
        $isi['title'] = 'Edit Data User';
        $this->load->view('dashboard/inc/header_view',$isi);
        $this->load->view('dashboard/edit_user_view');
        $this->load->view('dashboard/inc/footer_view');
    }
    
    public function delete_user($id)
    {
        $this->load->model('user_model');
        $this->user_model->delete_user($id);
    }
    
    public function edit_profil()
    {
        $isi['title'] = 'Edit Profil';
        $this->load->view('dashboard/inc/header_view',$isi);
        $this->load->view('dashboard/edit_profil_view');
        $this->load->view('dashboard/inc/footer_view');
    }
    
    public function data_apus()
    {
        $this->load->model('data_model');
        $isi['title'] = 'List Anak Putus Sekolah';
        if($this->data_model->get_all_apus() == FALSE)
        {
            $this->session->set_flashdata('danger','Tidak Ada Data Yang Tersimpan!!!');
            redirect('dashboard');
        }
        else
        {
            $isi['data'] = $this->data_model->get_all_apus();   
            $this->load->view('dashboard/inc/header_view',$isi);
            $this->load->view('dashboard/data_apus_view');
            $this->load->view('dashboard/inc/footer_view');
        }
    }
    
    public function data_user()
    {
        $this->load->model('user_model');
        $isi['title'] = 'List User';
        if($this->user_model->get_users() == FALSE)
        {
            $this->session->set_flashdata('danger','Tidak Ada Data Yang Tersimpan!!!');
            redirect('dashboard');
        }
        else
        {
            $isi['data'] = $this->user_model->get_users();   
            $this->load->view('dashboard/inc/header_view',$isi);
            $this->load->view('dashboard/data_user_view');
            $this->load->view('dashboard/inc/footer_view');
        }
    }
    
    public function konfirmasi_user()
    {
        $isi['title'] = 'List User';
        $this->load->view('dashboard/inc/header_view',$isi);
        $this->load->view('dashboard/konfirmasi_user_view');
        $this->load->view('dashboard/inc/footer_view');
        //$this->load->model('user_model');
        //$isi['users'] = $this->user_model->get_users();
    }
    
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }
    
}
