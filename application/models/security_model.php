<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Security_model extends CI_Model{

    public function getAdminSecurity()
    {
        $role = $this->session->userdata('role');
        if($role != 'admin')
        {
            $danger = $this->session->set_flashdata('danger','WOOOPPSS!!! TRYING TO ACCESS ADMIN SESSION WITHOUT LOGIN!!!?? YOU HAVE NO POWER HERE!!! PLEASE LOG IN FIRST');
            redirect('home');
            return $danger;
        }
    }
    
    public function getUserSecurity()
    {
        $role = $this->session->userdata('role');
        if($role != 'user')
        {
            $danger = $this->session->set_flashdata('danger','WOOOPPSS!!! TRYING TO ACCESS USER SESSION WITHOUT LOGIN!!!?? YOU HAVE NO POWER HERE!!! PLEASE LOG IN FIRST');
            redirect('home');
            return $danger;
        }
    }
    
}
