<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    public function get_prov()
    {
        $query = $this->db->get('prov');
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        if(($query->num_rows() < 1) OR ($query->num_rows() == 0))
        {
            return FALSE;
        }
    }
    
    public function get_kota($prov_id)
    {
        $this->db->where('prov_id', $prov_id);
        $query = $this->db->get('kab_kota');
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        if(($query->num_rows() < 1) OR ($query->num_rows() == 0))
        {
            return FALSE;
        }
    }
    
    public function get_kecamatan($kab_kota_id = '')
    {
        $this->db->where('kab_kota_id', $kab_kota_id);
        $query = $this->db->get('kecamatan');
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        if(($query->num_rows() < 1) OR ($query->num_rows() == 0))
        {
            return FALSE;
        }
    }
    
    public function get_all_apus()
    {
        $query = $this->db->get('bio_anak');
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        if(($query->num_rows() < 1) OR ($query->num_rows() == 0))
        {
            return FALSE;
        }
    }
    
    public function get_apus_by_prov($prov_id)
    {
        $this->db->where('prov_id',$prov_id);
        $query = $this->db->get('bio_anak');
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        if(($query->num_rows() < 1) OR ($query->num_rows() == 0))
        {
            return FALSE;
        }
    }
    
    public function get_apus_by_kota($kab_kota_id)
    {
        $this->db->where('kab_kota_id',$kab_kota_id);
        $query = $this->db->get('bio_anak');
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        if(($query->num_rows() < 1) OR ($query->num_rows() == 0))
        {
            return FALSE;
        }
    }
    
    public function get_apus_by_kecamatan($kecamatan_id)
    {
        $this->db->where('kecamatan_id',$kecamatan_id);
        $query = $this->db->get('bio_anak');
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        if(($query->num_rows() < 1) OR ($query->num_rows() == 0))
        {
            return FALSE;
        }
    }

    public function create_apus($data)
    {
        $this->db->insert('bio_anak',$data);
        return $this->db->insert_id();
    }
    
    public function delete_apus($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('bio_anak');
        redirect('dashboard/data_apus');
    }
    
    public function upload_apus_pict()
    {
            //$uid = $this->session->userdata('user_id');
            $config = array(
            'file_name' => $apus_id,
            'file_ext_tolower' => TRUE,
            'upload_path' => './photo/apus/',
            'allowed_types' => 'jpg|jpeg',
            'overwrite' => TRUE,
            'max_size' => '512000', // Maksimal ukuran file 512 KB
            'max_height' => '1500',
            'max_width' => '1000'
            );

            $this->upload->initialize($config);
            if($this->upload->do_upload())
            {
                $data = array('upload_data' => $this->upload->data());
                echo 'Sukses!!!';
//                $role = $this->session->userdata('role');
//                if($role == 'admin')
//                {
//                    redirect('dashboard/profile');
//                }
            }
            else
            {
                $error = $this->upload->display_errors();
                echo $error;
            }
    }
}
