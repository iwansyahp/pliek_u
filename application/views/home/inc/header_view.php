<!DOCTYPE html>
<html>
  <head>
    <!-- CSS Header -->
    <link rel="Shortcut Icon" href="<?=base_url() ?>/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<?=base_url() ?>template/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url() ?>template/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url() ?>template/plugins/iCheck/square/blue.css">
    <title><?php echo $title; ?></title>
  </head>
  <body class="hold-transition login-page">
      <div class="wrapper">
      <h2><?php echo $title; ?></h2>
      <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=base_url(); ?>home">Home</a>
		  <a class="navbar-brand" href="<?=base_url(); ?>home/statistik">Data Anak Putus Sekolah</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?=base_url(); ?>home/registrasi">Register</a></li>
            <li><a href="<?=base_url(); ?>home/login">Login</a></li>
          </ul>
        </div>
      </div>
    </div>
