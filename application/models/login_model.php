<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getLogin($username, $password)
    {
        $pwd = hash('sha256', $password . WEUW);
        $this->db->where('username',$username);
        $this->db->where('password',$pwd);
        $query = $this->db->get('user');
        
        if($query->num_rows()== 1)
        {
            foreach ($query->result() as $row)
            {
                $sess = array   ('username'     => $row->username,             
                                 'password'     => $row->password,
                                 'nama_lengkap'  => $row->nama_lengkap,                    
                                 'role'         => $row->role);
                if($row->role == 'admin'){
                    $this->session->set_userdata($sess);
                    redirect('dashboard');
                }

                if($row->role == 'user')
                {
                    $this->session->set_userdata($sess);
                    redirect('userboard');
                }

            }
        }
        else
        {
            $this->session->set_flashdata('info','Maaf, Username atau Password yang Anda Masukkan Salah.');
            redirect('home/login');
        }
    }
}
