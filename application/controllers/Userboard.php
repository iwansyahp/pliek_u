<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('security_model');
       // $this->security_model->getUserSecurity();
    }

    
    public function index()
    {
        $this->load->view('userboard/inc/header_view');
        $this->load->view('userboard/userboard_view');
        $this->load->view('userboard/inc/footer_view');
    }
    public function profil()
    {
        $this->load->view('userboard/inc/header_view');
        $this->load->view('userboard/profil_view');
        $this->load->view('userboard/inc/footer_view');
    }
    public function daftar_apus()
    {
        $this->load->model('data_model');
        $isi['provinsi'] = $this->data_model->get_prov();
        $this->load->view('userboard/inc/header_view');
        $this->load->view('userboard/form_daftar_apus_view',$isi);
        $this->load->view('userboard/inc/footer_view');
    }
    public function edit_apus()
    {
        $this->load->view('userboard/inc/header_view');
        $this->load->view('userboard/edit_apus_view');
        $this->load->view('userboard/inc/footer_view');
    }
    public function lihat_apus(){
        $this->load->view('userboard/inc/header_view');
        $this->load->view('userboard/data_apus_user_view');
        $this->load->view('userboard/inc/footer_view');   
    }
    
    public function get_kabupaten()
    {
        $prov_id = $this->input->post('prov_id');
        $this->db->where('prov_id',$prov_id);
        $q = $this->db->get('kab_kota');
        echo '<option>Pilih Kabupaten</option>';
        foreach($q->result() as $kota)
        {
            echo '<option value="'.$kota->id.'">'.$kota->kab_kota_name.'</option>';
        }
    }
    
    public function get_kecamatan()
    {
        $kota_id = $this->input->post('kota_id');
        $this->db->where('kab_kota_id',$kota_id);
        $q = $this->db->get('kecamatan');
        echo '<option>Pilih Kecamatan</option>';
        foreach($q->result() as $kec)
        {
            echo '<option value="'.$kec->id.'">'.$kec->kec_name.'</option>';
        }
    }
    
    public function list_apus()
    {
        
    }
    
    public function simpan_apus()
    {
        $nama_lengkap = $this->input->post('nama_lengkap');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tanggal_lahir = $this->input->post('tanggal_lahir');
        $prov_id = $this->input->post('prov_id');
        $kabupaten_id = $this->input->post('kabupaten_id');
        $kecamatan_id = $this->input->post('kecamatan_id');
        
        $data = array(
            'nama_lengkap' => $nama_lengkap,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir,
            'prov_id' => $prov_id,
            'kab_kota_id' => $kabupaten_id,
            'kec_id' => $kecamatan_id
        );
        $this->load->model('data_model'); 
        $apus_id = $this->data_model->create_apus($data);
        //echo $apus_id;
        //$this->data_model->upload_apus_pict();
    }
    
    public function cetak_data()
    {
        //...
    }
    
    public function profile()
    {
        //...
    }
    
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }
    
}
