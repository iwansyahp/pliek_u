         <ul class="sidebar-menu">
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard">
                <i class="fa fa-dashboard">
                </i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/konfirmasi_user">
                <i class="fa fa-th"></i>
                <span>Konfirmasi Pendaftaran</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/daftar_user">
                <i class="fa fa-edit"></i> <span>Pendaftaran</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url() ?>dashboard/daftar_user"><i class="fa fa-circle-o"></i> User</a></li>
                <li><a href="<?=base_url() ?>dashboard/daftar_apus"><i class="fa fa-circle-o"></i> Anak Putus Sekolah</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <a href="<?=base_url() ?>dashboard/data_user">
                <i class="fa fa-table"></i> <span>List Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url() ?>dashboard/data_user"><i class="fa fa-circle-o"></i> User</a></li>
                <li class="active"><a href="<?=base_url() ?>dashboard/data_apus"><i class="fa fa-circle-o"></i> Anak Putus Sekolah</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/logout">
                <i class="fa fa-share"></i>
                <span>Log Out</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Anak Putus Sekolah yang Terdaftar <br />
            <small>Relawan hanya dapat melihat Anak Putus Sekolah yang ia daftarkan</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Edit</a></li>
            <li class="active">Data Anak Putus Sekolah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Table With Full Features</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Lengkap</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal Lahir</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Kecamatan</th>
                        <th>Operasi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                          <?php foreach($data as $row); ?>
                        <td  style="padding-top:16px;">1</td>
                        <td>
                            <img src="<?=base_url() ?>template/dist/img/user2-160x160.jpg" width="40px" class="img-circle" alt="User Image">
                            <span> <?php echo $row->nama_lengkap; ?></span>
                        </td>
                        <td style="padding-top:16px;"><?php echo $row->tempat_lahir; ?></td>
                        <td style="padding-top:16px;"><?php echo $row->tanggal_lahir; ?></td>
                        <td style="padding-top:16px;"><?php echo $row->prov_id; ?></td>
                        <td style="padding-top:16px;"><?php echo $row->kab_kota_id; ?></td>
                        <td style="padding-top:16px;"><?php echo $row->kec_id; ?></td>
                        <td style="padding-top:13px;">
                            <a href="edit"> <img src="<?=base_url() ?>/images/icon_edit.png" width="30px" alt="Edit" title="Edit">  </a> <a href="<?=base_url() ?>dashboard/delete_apus/<?php echo $row->id; ?>"><img src="<?=base_url() ?>/images/icon_delete.png" width="30px" alt="Delete" title="Delete"></a>
                        </td>

                      </tr>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <script src="<?=base_url() ?>template/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url() ?>template/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url() ?>template/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>