
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data APUS yang Terdaftar
            <small>Relawan hanya dapat melihat APUS yang ia daftarkan</small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Table With Full Features</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                      <label>Tampilkan 
                        <select name="example1_length" aria-controls="example1" class="form-control input-sm">
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                          <option value="100">100</option>
                        </select> entri
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div id="example1_filter" class="dataTables_filter">
                      <label>Cari:<input type="search" class="form-control input-sm" placeholder="" aria-controls="example1"></label>
                </div>
              </div>
            </div>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Lengkap</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal Lahir</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Kecamatan</th>
                        <th>Operasi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td><a href="#">Shahrukh Khan</a></td>
                        <td>Mumbai</td>
                        <td>19/09/1888</td>
                        <td>Aceh</td>
                        <td>Aceh Besar</td>
                        <td>Jantho</td>
                        <td>
                          <div align="center">
                            <a href="<?=base_url()?>userboard/edit_apus"><button type="button" class="btn btn-warning btn-flat" action><i class="fa fa-pencil"></i></button></a>
                            <a href="<?=base_url()?>userboard/hapus_apus"><button type="button" class="btn btn-danger btn-flat" action><i class="fa fa-close"></i></button></a>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td><a href="#">Benyamin S</a></td>
                        <td>Jakarta</td>
                        <td>19/09/1288</td>
                        <td>Jak</td>
                        <td>Jak Besar</td>
                        <td>Jayapura</td>
                        <td>
                        <div align="center">
                          <a href="<?=base_url()?>userboard/edit_apus"><button type="button" class="btn btn-warning btn-flat" action><i class="fa fa-pencil"></i></button></a>
                          <a href="<?=base_url()?>userboard/hapus_apus"><button type="button" class="btn btn-danger btn-flat" action><i class="fa fa-close"></i></button></a>
                        </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <script src="<?=base_url() ?>template/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url() ?>template/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url() ?>template/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>