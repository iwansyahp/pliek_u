<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    public function get_users()
    {
        $query = $this->db->get('user');
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        if(($query->num_rows() < 1) OR ($query->num_rows() == 0))
        {
            return FALSE;
        }
    }
    
    public function insert_user($data)
    {
        $this->db->insert('user', $data);
    }
    
    public function update_user($id,$data)
    {
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }
    
    public function delete_user($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user');
        redirect('dashboard/data_user');
    }
}
