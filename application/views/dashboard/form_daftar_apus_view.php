        <ul class="sidebar-menu">
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard">
                <i class="fa fa-dashboard">
                </i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/konfirmasi_user">
                <i class="fa fa-th"></i>
                <span>Konfirmasi Pendaftaran</span>
              </a>
            </li>
            <li class="active treeview">
              <a href="<?=base_url() ?>dashboard/daftar_user">
                <i class="fa fa-edit"></i> <span>Pendaftaran</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url() ?>dashboard/daftar_user"><i class="fa fa-circle-o"></i> User</a></li>
                <li class="active"><a href="<?=base_url() ?>dashboard/daftar_apus"><i class="fa fa-circle-o"></i> Anak Putus Sekolah</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/data_user">
                <i class="fa fa-table"></i> <span>List Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url() ?>dashboard/data_user"><i class="fa fa-circle-o"></i> User</a></li>
                <li><a href="<?=base_url() ?>dashboard/data_apus"><i class="fa fa-circle-o"></i> Anak Putus Sekolah</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/logout">
                <i class="fa fa-share"></i>
                <span>Log Out</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Pendaftaran User
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Pendaftaran</a></li>
            <li class="active">User</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Pendaftaran</h3>
            </div><!-- /.box-header -->
            <form role="form">
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="inputNamaLengkap">Nama Lengkap</label>
                      <input name= "nama_lengkap" type="text" class="form-control" id="nama_lengkap" placeholder="Masukkan nama lengkap">
                    </div>
                    <div class="form-group">
                      <label for="inputNamaLengkap">Tempat Lahir</label>
                      <input name= "tempat_lahir" type="text" class="form-control" id="tempat_lahir" placeholder="Masukkan tempat lahir">
                    </div>
                    <div class="form-group">
                      <label for="inputNamaLengkap">Tanggal Lahir</label>
                      <input name= "tanggal_lahir" type="date" class="form-control" id="tanggal_lahir" placeholder="Masukkan tanggal lahir">
                    </div>
                    <div class="form-group">
                      <label>Provinsi</label>
                      <select name = "provinsi" class="form-control">
                        <option>Provinsi</option>
                      </select>
                    </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                      <label>Kabupaten</label>
                      <select name = "kabupaten" class="form-control">
                        <option>Kabupaten</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Kecamatan</label>
                      <select name = "kecamatan" class="form-control">
                        <option>Kecamatan</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Foto Anak</label>
                      <input type="file" id="photo_loc">
                      <p class="help-block">Pilih foto anak yang ingin dimasukkan.</p>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Data yang saya masukkan adalah benar adanya
                      </label>
                    </div>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
                  <div class="box-footer float-right">
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div>
                </form>
          </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


