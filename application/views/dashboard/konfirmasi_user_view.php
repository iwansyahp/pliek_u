         <ul class="sidebar-menu">
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard">
                <i class="fa fa-dashboard">
                </i> <span>Dashboard</span>
              </a>
            </li>
            <li class="active treeview">
              <a href="<?=base_url() ?>dashboard/konfirmasi_user">
                <i class="fa fa-th"></i>
                <span>Konfirmasi Pendaftaran</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/daftar_user">
                <i class="fa fa-edit"></i> <span>Pendaftaran</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url() ?>dashboard/daftar_user"><i class="fa fa-circle-o"></i> User</a></li>
                <li><a href="<?=base_url() ?>dashboard/daftar_apus"><i class="fa fa-circle-o"></i> Anak Putus Sekolah</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/data_user">
                <i class="fa fa-table"></i> <span>List Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url() ?>dashboard/data_user"><i class="fa fa-circle-o"></i> User</a></li>
                <li><a href="<?=base_url() ?>dashboard/data_apus"><i class="fa fa-circle-o"></i> Anak Putus Sekolah</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/logout">
                <i class="fa fa-share"></i>
                <span>Log Out</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Konfirmasi Pendaftaran User<br />
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url() ?>dashboard""><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=base_url() ?>dashboard/konfirmasi_user">Konfirmasi Pendaftaran</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data User yang Terdaftar</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Lengkap</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal Lahir</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Kecamatan</th>
                        <th>Operasi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>
                            <a href="">
                            <img src="<?=base_url() ?>template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                            <span class="hidden-xs">Alexander Pierce</span>
                            </a>
                        </td>
                        <td>Mumbai</td>
                        <td>19/09/1888</td>
                        <td>Aceh</td>
                        <td>Aceh Besar</td>
                        <td>Jantho</td>
                        
                        <td>
                          <ul>
                            <li><a href="Edit"></a></li>
                          </ul>
                        </td>

                      </tr>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <script src="<?=base_url() ?>template/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url() ?>template/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url() ?>template/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>