        <ul class="sidebar-menu">
            <li class="active treeview">
              <a href="<?=base_url() ?>dashboard">
                <i class="fa fa-dashboard">
                </i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/konfirmasi_user">
                <i class="fa fa-th"></i>
                <span>Konfirmasi Pendaftaran</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/daftar_user">
                <i class="fa fa-edit"></i> <span>Pendaftaran</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url() ?>dashboard/daftar_user"><i class="fa fa-circle-o"></i> User</a></li>
                <li><a href="<?=base_url() ?>dashboard/daftar_apus"><i class="fa fa-circle-o"></i> Anak Putus Sekolah</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/data_user">
                <i class="fa fa-table"></i> <span>List Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url() ?>dashboard/data_user"><i class="fa fa-circle-o"></i> User</a></li>
                <li><a href="<?=base_url() ?>dashboard/data_apus"><i class="fa fa-circle-o"></i> Anak Putus Sekolah</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?=base_url() ?>dashboard/logout">
                <i class="fa fa-share"></i>
                <span>Log Out</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Profil
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Edit</a></li>
            <li class="active">Profil</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Profil</h3>
            </div><!-- /.box-header -->
            <form role="form">
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nama Lengkap</label>
                      <input type="nama_lengkap" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                      <input type="username" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Tempat Lahir</label>
                      <input type="tempat_lahir" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                      <label>Tanggal Lahir</label>
                        <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                            <input type="date" class="form-control pull-right" id="reservation">
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->
                    <div class="form-group">
                      <label for="exampleInputPassword1">Alamat Lengkap</label>
                      <input type="alamat_lengkap" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nomor Hp</label>
                      <input type="no_hp" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Input Foto</label>
                      <input type="file" id="exampleInputFile">
                    </div>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
                  <div class="box-footer float-right">
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div>
                </form>
          </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


