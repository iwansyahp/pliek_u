<div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Profil Pengguna
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            
              <!-- About Me Box -->
              <div class="box box-primary">
              <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
                  <h2 class="profile-username text-center">[Nina Mcintire]</h2>
                  <h4 class="text-muted text-center">[Relawan]</h4>
                </div><!-- /.box-body -->
                <div class="box-header with-border">
                  <h3 class="box-title">Tentang Saya</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa fa-map-o margin-r-5"></i>  Tempat Lahir</strong>
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>
                  <hr>
                  <strong><i class="fa fa-calendar-times-o margin-r-5"></i>  Tanggal Lahir</strong>
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>
                  <hr>
                  <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>
                  <p class="text-muted">Malibu, California</p>
                  <hr>
                  <hr>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>