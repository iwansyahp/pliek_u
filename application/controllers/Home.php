<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function index()
    {
        $isi['title'] = 'Home Page';
        $this->load->view('home/inc/header_view',$isi);
        $this->load->view('home/home_view');
        $this->load->view('home/inc/footer_view');
    }
    
    public function login()
    {
        $isi['title'] = 'Login Page';
        $this->load->view('home/inc/header_view',$isi);
        $this->load->view('home/form_login_view');
        $this->load->view('home/inc/footer_view');
    }
    
    public function do_login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $this->input->post('username');
        $this->load->model('login_model');
        $this->login_model->getLogin($username, $password);
    }
    
    public function registrasi()
    {
        $isi['title'] = 'Register Page';
        $this->load->view('home/inc/header_view',$isi);
        $this->load->view('home/form_registrasi_view');
        $this->load->view('home/inc/footer_view');
    }
    
    public function daftar_user()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $role = 'user';
        $nama_lengkap = $this->input->post('nama_lengkap');
        
        $pwd = hash('sha256', $password . WEUW);
        $data = array(
            'username' => $username,
            'password' => $pwd,
            'role' => $role,
            'nama_lengkap' => $nama_lengkap
        );
        $this->load->model('user_model'); 
        $this->user_model->insert_user($data);
        redirect('home/login');
        $isi['title'] = 'Registrasi';
        $this->load->view('home/form_registrasi_view',$isi);
    }
    
    public function statistik()
    {
        $isi['title'] = 'Data Statistik Anak Putus Sekolah';
        $this->load->view('home/inc/header_view',$isi);
        $this->load->view('home/statistik_view');
        $this->load->view('home/inc/footer_view');
    }
    
//    public function contact()
//    {
//        echo 'Tampil Halaman contact Us';
//    }
    
    
}
